package test.com.floatingpointpoc.service;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by umesh on 21/11/16.
 */

public class BoundService extends Service {

    private String TAG = BoundService.class.getSimpleName();
    private IBinder iBinder;

    @Override
    public void onCreate() {
        super.onCreate();
        iBinder = new MyBinder();
        Log.d(TAG, "onCreate");
    }

    private Bitmap bitmap;
    ArrayList<Bitmap> data = new ArrayList<>();

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        return START_STICKY;
    }

    private void dd(Intent intent) {
        final String a[] = (String[]) intent.getExtras().getStringArray("URL");

        for (int i = 0; i < a.length; i++) {
            final int finalI = i;
            try {
                URL u = new URL(a[finalI]);
                InputStream inputStream = null;
                inputStream = u.openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
                data.add(bitmap);
                Log.i(TAG, i + " : " + bitmap.toString());
            } catch (Exception e) {

            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(final Intent intent) {
        Log.d(TAG, "onBind");
        new Thread(new Runnable() {
            @Override
            public void run() {
                dd(intent);
            }
        }).start();
        return iBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.d(TAG, "onRebind");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    public class MyBinder extends Binder {
        public BoundService getService() {
            return BoundService.this;
        }
    }

    public ArrayList<Bitmap> getList() {
        return data;
    }
}

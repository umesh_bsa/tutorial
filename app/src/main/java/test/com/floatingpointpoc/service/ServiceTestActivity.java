package test.com.floatingpointpoc.service;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import test.com.floatingpointpoc.R;

public class ServiceTestActivity extends AppCompatActivity {

    private String TAG = ServiceTestActivity.class.getSimpleName();
    String[] ur = {
            "http://farm8.staticflickr.com/7315/9046944633_881f24c4fa_s.jpg",
            "http://farm4.staticflickr.com/3777/9049174610_bf51be8a07_s.jpg",
            "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg",
            "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg", "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg", "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg", "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg", "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg",
            "http://farm3.staticflickr.com/2828/9046946983_923887b17d_s.jpg"};
    private ListView listView;
    private BoundService boundService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);


        listView = (ListView) this.findViewById(R.id.list);

        findViewById(R.id.btn_normal_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(ServiceTestActivity.this, NormalService.class);
                i.putExtra("DATA", "Activity to Service");
                i.putExtra("RESULT", new ResultReceiver(new Handler()) {
                    @Override
                    protected void onReceiveResult(int resultCode, Bundle resultData) {
                        if (resultCode == Activity.RESULT_OK) {
                            Log.i(TAG, " Data : " + resultData.get("DATA"));
                        } else {
                            // ... show UI for unsuccessful login ...
                        }
                    }
                });
                startService(i);
            }
        });

        findViewById(R.id.btn_bound_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ServiceTestActivity.this, BoundService.class);
                i.putExtra("URL", ur);
                bindService(i, new SConnect(), Context.BIND_AUTO_CREATE);
            }
        });


        findViewById(R.id.btn_intent_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ServiceTestActivity.this, IntentServiceTest.class);
                intent.putExtra("RESULT", new ResultReceiver(new Handler()) {
                            @Override
                            protected void onReceiveResult(int resultCode, Bundle resultData) {
                                if (resultCode == Activity.RESULT_OK) {
                                    ArrayList<Bitmap> arrayList = (ArrayList<Bitmap>) resultData.getSerializable("DATA");
                                    Log.i("DATA", arrayList.toString());
                                    // ImageAdapter imageAdapter = new ImageAdapter(this, R.layout.image_adapter, arrayList);
                                    //  listView.setAdapter(imageAdapter);
                                }
                            }
                        }

                );
                intent.putExtra("URL", ur);
                startService(intent);
            }
        });

        findViewById(R.id.btn_handler_msg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult");
    }


    public class SConnect implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            boundService = ((BoundService.MyBinder) iBinder).getService();
            Log.d(TAG, boundService.getList().toString());

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    }

}

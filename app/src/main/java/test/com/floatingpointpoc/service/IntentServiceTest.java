package test.com.floatingpointpoc.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by umesh on 21/11/16.
 */

public class IntentServiceTest extends IntentService {

    private String TAG = IntentServiceTest.class.getSimpleName();
    private Bitmap bitmap;

    public IntentServiceTest() {
        super("IntentServiceTest");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    private ResultReceiver receiver;

    ArrayList<Bitmap> data = new ArrayList<>();

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent");

        receiver = intent.getExtras().getParcelable("RESULT");
        final String a[] = (String[]) intent.getExtras().getStringArray("URL");

        for (int i = 0; i < a.length; i++) {
            final int finalI = i;
            try {
                URL u = new URL(a[finalI]);
                InputStream inputStream = null;
                inputStream = u.openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
                data.add(bitmap);
            } catch (Exception e) {

            }
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable("DATA", data);
        receiver.send(Activity.RESULT_OK, bundle);
    }
}

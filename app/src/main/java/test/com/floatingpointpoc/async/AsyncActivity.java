package test.com.floatingpointpoc.async;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import java.io.InputStream;
import java.net.URL;

import test.com.floatingpointpoc.R;

public class AsyncActivity extends AppCompatActivity {


    String[] ur = {
            "http://farm8.staticflickr.com/7315/9046944633_881f24c4fa_s.jpg",
            "http://farm4.staticflickr.com/3777/9049174610_bf51be8a07_s.jpg",
            "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg",
            "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg", "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg", "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg", "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg", "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg",
            "http://farm3.staticflickr.com/2828/9046946983_923887b17d_s.jpg"};


    private TextView tvData;
    private Bitmap bitmap;
    private ListView listView;
    private TextView tvLibData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.async_activity);
        tvLibData = (TextView) findViewById(R.id.tv_lib_data);

       /* TestAarClass aarClass = new TestAarClass();
        aarClass.setName("AAR file");



        tvLibData.setText(aarClass.getName());

        listView = (ListView) this.findViewById(R.id.list);
        findViewById(R.id.btn_async).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageAdapter imageAdapter = new ImageAdapter(AsyncActivity.this, R.layout.image_adapter, ur);
                listView.setAdapter(imageAdapter);

            }
        });*/
    }

    int i;


    public class ImageAdapter extends ArrayAdapter<String> {
        private String[] imageURLArray;
        private LayoutInflater inflater;

        public ImageAdapter(Context context, int textViewResourceId, String[] imageArray) {
            super(context, textViewResourceId, imageArray);
            inflater = ((Activity) context).getLayoutInflater();
            imageURLArray = imageArray;
        }

        class ViewHolder {
            ImageView imageView;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.image_adapter, null);

                viewHolder = new ViewHolder();
                viewHolder.imageView = (ImageView) convertView.findViewById(R.id.image_view);
                convertView.setTag(viewHolder);
            }

            viewHolder = (ViewHolder) convertView.getTag();
            new DownloadAsyncTask(viewHolder, imageURLArray[position]).execute();
            return convertView;
        }
    }

    class DownloadAsyncTask extends AsyncTask<String, String, Bitmap> {
        private String url;
        private ImageAdapter.ViewHolder viewHolder;

        public DownloadAsyncTask(ImageAdapter.ViewHolder viewHolder, String url) {
            this.viewHolder = viewHolder;
            this.url = url;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            Log.i("DATA", "bg " + i);
            try {
                URL u = new URL(url);
                InputStream inputStream = u.openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap b) {
            super.onPostExecute(b);
            viewHolder.imageView.setImageBitmap(b);
        }
    }
}

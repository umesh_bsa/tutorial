package test.com.floatingpointpoc.broadcast;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import test.com.floatingpointpoc.R;

public class BroadcastActivity extends AppCompatActivity {

    private String TAG = BroadcastActivity.class.getSimpleName();
    private String ACTION_DYNAMIC_BROADCAST = "ACTION_DYNAMIC_BROADCAST";
    private DynamicBroadcast dynamicBroadcas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast);
        dynamicBroadcas = new DynamicBroadcast();

        final IntentFilter intentFilter = new IntentFilter(ACTION_DYNAMIC_BROADCAST);
        registerReceiver(dynamicBroadcas, intentFilter);

        findViewById(R.id.btn_static_bdcst).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BroadcastActivity.this, StaticBroadcast.class);
                sendBroadcast(i);
            }
        });

        findViewById(R.id.btn_dynamic_bdcst).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ACTION_DYNAMIC_BROADCAST);
                sendBroadcast(i);
            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (dynamicBroadcas != null)
            unregisterReceiver(dynamicBroadcas);
    }
}

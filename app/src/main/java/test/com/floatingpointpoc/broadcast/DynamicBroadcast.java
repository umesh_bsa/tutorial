package test.com.floatingpointpoc.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class DynamicBroadcast extends BroadcastReceiver {


    private static final String TAG = DynamicBroadcast.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Dynamic");
        Toast.makeText(context, "Dynamic float ", Toast.LENGTH_LONG).show();
    }
}

package test.com.floatingpointpoc.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class StaticBroadcast extends BroadcastReceiver {


    private static final String TAG = StaticBroadcast.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Static");
        Toast.makeText(context, "Static ", Toast.LENGTH_LONG).show();
    }
}

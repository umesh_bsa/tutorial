package test.com.floatingpointpoc.permission;

public interface PermissionListener {
    void granted(boolean isGranted);
}
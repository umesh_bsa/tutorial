package test.com.floatingpointpoc.permission;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import test.com.floatingpointpoc.R;

public class TransparentActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.transparent_activity);
    }
}
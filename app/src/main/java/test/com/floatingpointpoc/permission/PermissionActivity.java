package test.com.floatingpointpoc.permission;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import test.com.floatingpointpoc.R;
import test.com.floatingpointpoc.activity.AppConstant;


public class PermissionActivity extends AppCompatActivity {

    private final String TAG = PermissionActivity.class.getSimpleName();
    private PermissionListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);
        findViewById(R.id.btn_location_enable).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                 startActivity(new Intent(PermissionActivity.this, TransparentActivity.class));


              /*  checkContactPermission(new PermissionListener() {
                    @Override
                    public void granted(boolean isGranted) {
                        if (isGranted) {
                            Log.i(TAG, "is Granted");
                            getCursor("u");
                        }
                    }
                });*/
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TSTAct", "#onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("TSTAct", "#onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TSTAct", "#onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("TSTAct", "#onStart");
    }

    private Cursor cursor;

    private Cursor getCursor(String searchString) {
        Log.i(TAG, "getCursor");
        String selection;
        if (TextUtils.isEmpty(searchString)) {
            selection = ContactsContract.Data.HAS_PHONE_NUMBER + "!=0 AND (" + ContactsContract.Data.MIMETYPE + "=?)";
        } else {
            selection = ContactsContract.Data.HAS_PHONE_NUMBER + "!=0 AND (" + ContactsContract.Data.MIMETYPE + "=?) AND (" + ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " LIKE '%" + searchString + "%' OR " + ContactsContract.Contacts.Data.DATA1 + " LIKE '" + searchString + "%')";
        }
        cursor = getContentResolver().query(
                ContactsContract.Data.CONTENT_URI,
                null,
                selection,
                new String[]{ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE},
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE ASC");
        return cursor;
    }

    private void checkContactPermission(PermissionListener listener) {
        this.listener = listener;
        String[] whatPermission = {Manifest.permission.READ_CONTACTS};
        if ((ContextCompat.checkSelfPermission(this, whatPermission[0]) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this,
                    whatPermission,
                    AppConstant.REQUEST_CODE.CONTACT_PERMISSION);
            Log.i(TAG, "Is check");
            listener.granted(false);
        } else {
            Log.i(TAG, "Is  not check");
            listener.granted(true);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i(TAG, "onRequestPermissionsResult ");
        switch (requestCode) {
            case AppConstant.REQUEST_CODE.CONTACT_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (listener != null) {
                        listener.granted(true);
                    }
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
                        String[] whatPermission = {Manifest.permission.READ_CONTACTS};
                        ActivityCompat.requestPermissions(this,
                                whatPermission,
                                AppConstant.REQUEST_CODE.CONTACT_PERMISSION);
                    } else {
                        showDialogOK("Contact permission is required",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                dialog.dismiss();
                                                gotoSettings();
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                break;
                                        }
                                    }
                                });
                    }
                }
                break;
        }
    }

    private void gotoSettings() {
        Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        getApplicationContext().startActivity(i);
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setTitle(this.getString(R.string.app_name))
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
}

package test.com.floatingpointpoc.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

import test.com.floatingpointpoc.R;

/**
 * Created by umesh on 29/6/17.
 */

public class HandlerActivity extends AppCompatActivity {

    String[] ur = {
            "http://farm8.staticflickr.com/7315/9046944633_881f24c4fa_s.jpg",
            "http://farm4.staticflickr.com/3777/9049174610_bf51be8a07_s.jpg",
            "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg",
            "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg",
            "http://farm3.staticflickr.com/2828/9046946983_923887b17d_s.jpg"};
    private ImageView ivDownlod;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.d);
        ivDownlod = (ImageView) findViewById(R.id.iv_download);

        findViewById(R.id.btn_download).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downloadImage();
                    }
                });
    }

    private void downloadImage() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                URL u = null;
                InputStream inputStream = null;
                try {
                    u = new URL(ur[0]);
                    inputStream = u.openStream();
                } catch (Exception e) {
                    Log.i("DDDD", "Error " + e.getMessage());
                }
                final Bitmap bitmapImage = BitmapFactory.decodeStream(inputStream);

                Bundle b = new Bundle();
                b.putParcelable("DATA", bitmapImage);
                Message m = new Message();
                m.setData(b);
                h.sendMessage(m);

               /* new Handler(getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                     ivDownlod.setImageBitmap(bitmapImage);
                    }
                });*/
            }
        }).start();
    }

    Handler h = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bitmap m = msg.getData().getParcelable("DATA");
            ivDownlod.setImageBitmap(m);
        }
    };
}

package test.com.floatingpointpoc.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import test.com.floatingpointpoc.R;

public class ATextActivity extends AppCompatActivity {


    private Button aBtn;
    private String TAG = ATextActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a);

    }

}

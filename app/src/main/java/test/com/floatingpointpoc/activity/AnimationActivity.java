package test.com.floatingpointpoc.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import test.com.floatingpointpoc.R;
import test.com.floatingpointpoc.fragment.Fragment1;
import test.com.floatingpointpoc.fragment.FragmentA;
import test.com.floatingpointpoc.fragment.FragmentB;
import test.com.floatingpointpoc.fragment.FragmentC;

public class AnimationActivity extends AppCompatActivity {

    private static final String TAG = AnimationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        findViewById(R.id.btn_a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragmentAnim(R.id.container, new FragmentA());
            }
        });

        findViewById(R.id.btn_b).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragmentAnim(R.id.container, new FragmentB());
            }
        });

        findViewById(R.id.btn_c).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragmentAnim(R.id.container, new FragmentC());
            }
        });

        replaceFragment(R.id.container, new Fragment1());
    }

    public void replaceFragment(int containerId, Fragment fragment) {
        try {
            if (getSupportFragmentManager() != null) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
                fragmentTransaction.replace(containerId, fragment);
                fragmentTransaction.commit();
            }
        } catch (Exception e) {

        }
    }

    public void replaceFragmentAnim(int containerId, Fragment fragment) {
        try {
            if (getSupportFragmentManager() != null) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                if (fragment.isAdded()) {
                    Log.i(TAG, Fragment.class.getSimpleName());
                    fragmentTransaction.remove(fragment);
                    fragmentTransaction.commit();
                    getSupportFragmentManager().executePendingTransactions();
                }
                fragmentTransaction.setCustomAnimations(R.anim.animation_up, R.anim.animation_up, R.anim.animation_down, R.anim.animation_down);
                fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
                fragmentTransaction.replace(containerId, fragment);
                fragmentTransaction.commit();
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void setHeder(String t) {
        TextView tvHeader = (TextView) findViewById(R.id.tv_header);
        tvHeader.setText(t);

    }
}

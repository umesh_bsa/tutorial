package test.com.floatingpointpoc.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.VideoView;

import java.io.File;
import java.util.Calendar;

import test.com.floatingpointpoc.R;

/**
 * Created by umesh on 7/2/17.
 */

public class CameraActivity extends AppCompatActivity {

    private static final String TAG = test.com.floatingpointpoc.activity.AnimationActivity.class.getSimpleName();

    private String selectedImagePath;
    private ImageView imageView;
    private VideoView videoView;
    private int videoGallery = 100;
    private int videoCapture = 200;
    private int openCameraImage = 300;
    private int openGalleryImage = 400;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        imageView = (ImageView) findViewById(R.id.image);
        videoView = (VideoView) findViewById(R.id.video);

        findViewById(R.id.btn_m_gallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoGallery();
            }


        });

        findViewById(R.id.btn_m_capture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoGallery();
            }


        });
        findViewById(R.id.btn_v_gallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoGallery();
            }


        });

        findViewById(R.id.btn_v_capture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoCapture();
            }
        });
    }

    private void videoGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_PICK);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(intent, videoGallery);
    }

    private void videoCapture() {
        selectedImagePath = Environment.getExternalStorageDirectory() + "/HoopBoostImage_" + Calendar.getInstance().getTimeInMillis() + ".mp4";
        File file = new File(selectedImagePath);
        Uri outputFileUri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() +
                ".provider", file);
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.setType("video/*");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(intent, videoCapture);
    }

    private void openCameraImage() {
        selectedImagePath = Environment.getExternalStorageDirectory() + "/HoopBoostImage_" + Calendar.getInstance().getTimeInMillis() + ".jpg";
        File file = new File(selectedImagePath);
        Uri outputFileUri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() +
                ".provider", file);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(intent, openCameraImage);
    }

    private void openGalleryImage() {
        Intent intent;
        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(intent, openGalleryImage);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("TAG", "Data " + data);
        videoView.setVideoURI(data.getData());
    }
}




package test.com.floatingpointpoc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import test.com.floatingpointpoc.R;
import test.com.floatingpointpoc.broadcast.BroadcastActivity;
import test.com.floatingpointpoc.fragment.FragmentTestActivity;
import test.com.floatingpointpoc.service.ServiceTestActivity;
import test.com.floatingpointpoc.test.CaptureSignatureActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.animation_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, CameraActivity.class);
                startActivity(i);
            }
        });

        findViewById(R.id.signature_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, CaptureSignatureActivity.class);
                startActivity(i);
            }
        });

        findViewById(R.id.sub_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, SubActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation_up, R.anim.no_change);
            }
        });

        findViewById(R.id.activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, A.class);
                startActivity(i);
            }
        });
        findViewById(R.id.broadcast).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, BroadcastActivity.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btn_fragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, FragmentTestActivity.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btn_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ServiceTestActivity.class);
                startActivity(i);
            }
        });
    }

}

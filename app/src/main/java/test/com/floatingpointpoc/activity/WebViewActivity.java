package test.com.floatingpointpoc.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import test.com.floatingpointpoc.R;

public class WebViewActivity extends AppCompatActivity {


    private WebView webView;
    private ProgressDialog progressDialog;
    private TextView tvHeader;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        webView = (WebView) findViewById(R.id.web_view);
        tvHeader = (TextView) findViewById(R.id.tv_header);
        findViewById(R.id.tv_header).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvHeader.setEnabled(false);
                setWebView();
            }
        });
    }

    private void setWebView() {
        String url = "https://makefastmooring.com";

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.loadUrl(url);
    }

    public class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showProgressBar();
        }

        public void onPageFinished(WebView view, String url) {
            tvHeader.setEnabled(true);
            hideProgressBar();
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }


    public void showProgressBar() {

        if (progressDialog == null) {
            progressDialog = ProgressDialog.show(WebViewActivity.this, "", "", true);
        }
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
package test.com.floatingpointpoc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import test.com.floatingpointpoc.R;

public class ActivityIntentFilter extends AppCompatActivity {


    private Button aBtn;
    private String TAG = ActivityIntentFilter.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_filter);

        findViewById(R.id.btn_browser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent("com.example.My_Application.LAUNCH");
                startActivity(i);

            }
        });

        findViewById(R.id.btn_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityIntentFilter.this, ATextActivity.class);
                startActivity(i);
            }
        });
    }
}

package test.com.floatingpointpoc.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import test.com.floatingpointpoc.R;

public class FragmentTestActivity extends AppCompatActivity {

    private String TAG = FragmentTestActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        Log.d(TAG, "onCreate");
        findViewById(R.id.add1_fragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentA fragmentA = new FragmentA();

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
               // transaction.addToBackStack(FragmentA.class.getSimpleName());
                transaction.replace(R.id.container1, fragmentA);
                transaction.commit();
            }
        });

        findViewById(R.id.add2_fragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment2 fragment2 = new Fragment2();

                FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
                //    transaction.addToBackStack(Fragment1.class.getSimpleName());
                transaction2.replace(R.id.container2, fragment2);
                transaction2.commit();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}

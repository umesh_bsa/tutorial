package test.com.floatingpointpoc.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import test.com.floatingpointpoc.R;

public class FragRemoveTestActivity extends AppCompatActivity {


    private Fragment1 myFrag;
    private List<Fragment1> myFragList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frag_remove_test);
        myFragList = new ArrayList<>();
        findViewById(R.id.btn_add_frag).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                myFrag = new Fragment1();
                myFragList.add(myFrag);
                fragmentTransaction.setCustomAnimations(R.anim.animation_up, R.anim.animation_down, R.anim.animation_up, R.anim.animation_down);
                fragmentTransaction.add(R.id.container_add_remove, myFrag);
                if (myFragList.size() % 2 == 0)
                    fragmentTransaction.addToBackStack(null);

                fragmentTransaction.commit();
            }
        });

        findViewById(R.id.btn_remove_frag).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((myFragList.size() - 1) >= 0) {
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    Fragment1 fragment1 = myFragList.get(myFragList.size() - 1);
                    fragmentTransaction.remove(fragment1);
                    myFragList.remove(fragment1);
                    fragmentTransaction.commit();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.i("TAG", "Count : " + getSupportFragmentManager().getBackStackEntryCount());
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }

    }


}

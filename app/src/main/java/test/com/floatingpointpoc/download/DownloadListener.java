package test.com.floatingpointpoc.download;


public interface DownloadListener {
    void progressUpdate(Message value);
}